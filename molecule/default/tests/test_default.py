import os
import re
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_nfs_service(host):
    nfs = host.service("nfs-server")
    assert nfs.is_enabled
    assert nfs.is_running


def test_rpcbind_service(host):
    rpc = host.service("rpcbind")
    assert rpc.is_enabled
    assert rpc.is_running


def test_etc_exports(host):
    exports = host.file('/etc/exports').content_string
    assert re.search(r'/export/epics\s+\*\(async,rw,insecure,crossmnt\)', exports) is not None
#    assert re.search(r'/export/startup\s+\*\(async,ro,no_root_squash,insecure,crossmnt\)', exports) is not None


def test_exports_directories(host):
    epics = host.file('/export/epics')
    assert epics.is_directory
    assert epics.user == 'root'
    assert epics.group == 'root'
    shared = host.file('/export/shared')
    assert shared.is_directory
    assert shared.user == 'nfsnobody'
    assert shared.group == 'nfsnobody'
    assert not host.file('/export/startup').exists
    assert not host.file('/export/sdk/eldk-5.6').exists
